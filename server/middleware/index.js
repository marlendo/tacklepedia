exports.failed = function (res, err) {
    console.log(err.message)
    res.status(200).json({
        error: true,
        message: err.message,
        data: false
    });
}

exports.success = function (res, data) {
    res.status(200).json({
        error: false,
        message: "success",
        data
    });
}