const axios = require('axios');
const midd = require('../middleware');
const { getColorFromURL } = require('color-thief-node');

const baseUrl = 'http://202.157.186.53:3000';

exports.initial = async function (req, res) {
    try {
        midd.success(res, 'welcome to middleware tacklepedia')
    } catch (error) {
        midd.failed(res, error)
    }
}

exports.tackepedia = async function (req, res) {
    try {
        const response = await axios({
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            url: `${baseUrl}/${req.params.id}`,
            data: req['body']
        })

        if (Number(response.data.status) < 300) {
            let data = response.data.values;
            if (data) {
                midd.success(res, data);
            }
        } else {
            throw response.data
        }
    } catch (error) {
        midd.failed(res, error)
    }
}

exports.inquiryAds = async function (req, res) {
    try {

        console.log('start')
        const response = await axios({
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            url: `${baseUrl}/inquiryIklan`,
            data: req['body']
        })

        if (Number(response.data.status) < 300) {
            let data = response.data.values;
            for (let row of data) {
                let imgSrc = `http://202.157.186.53/assets/images/upload/${row.gambar}`;
                console.log(`read color -> ${imgSrc}`);
                let dominantColor = await getColorFromURL(imgSrc)
                row.url = imgSrc;
                row.dominantColor = `rgb(${dominantColor[0]},${dominantColor[1]},${dominantColor[2]})`;
            }
            midd.success(res, data);
        }
    } catch (error) {
        midd.failed(res, error)
    }
}

exports.inquiryNewArrival = async function (req, res) {
    try {
        const response = await axios({
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'post',
            url: `${baseUrl}/inquiryNewArrival`,
            data: req['body']
        })

        if (Number(response.data.status) < 300) {
            let data = response.data.values;
            let newData = [];
            for (let row of data) {
                newData.push({
                    id: row.id_product,
                    sku: row.username_distributor,
                    name: row.nama_product,
                    price: row.harga_product,
                    discount: row.diskon_product,
                    offerEnd: null,
                    new: true,
                    rating: 5,
                    saleCount: 0,
                    category: [row.id_kategori, 'New Arrival'],
                    tag: ['sport', row.id_kategori, row.nama_product],
                    image: [
                        `http://202.157.186.53/assets/images/upload/${row.gambar}`,
                        `http://202.157.186.53/assets/images/upload/${row.gambar}`,
                    ],
                    shortDescription: row.deskripsi.length > 15 ? `${row.deskripsi.substring(0, 15)}...` : row.deskripsi,
                    fullDescription: row.deskripsi.length > 15 ? row.deskripsi : ''
                })
            }
            midd.success(res, newData);
        } else {
            midd.failed(res, response.data)
        }
    } catch (error) {
        midd.failed(res, error)
    }
}