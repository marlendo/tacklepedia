// const midd = require('../middlewares');
const ctrl = require('../controller');

module.exports = function (app) {

    // app.use((req, res, next) => {
    //     audit.logger(req)
    //     next()
    // })

    app.route('/api').get(ctrl.initial)
    app.route('/api/tackepedia/:id').post(ctrl.tackepedia)
    app.route('/api/inquiryAds').post(ctrl.inquiryAds)
    app.route('/api/inquiryNewArrival').post(ctrl.inquiryNewArrival)
   
};
