import CryptoJS from 'crypto-js';
import SecureStorage from 'secure-web-storage';
import moment from 'moment';

const SECRET_KEY = 'app.woo-wa.com';

const store = typeof window !== `undefined` ? window.localStorage : null

const secureStorage = new SecureStorage(store, {
    hash: function hash(key) {
        key = CryptoJS.SHA256(key, SECRET_KEY);

        return key.toString();
    },
    encrypt: function encrypt(data) {
        data = CryptoJS.AES.encrypt(data, SECRET_KEY);

        data = data.toString();

        return data;
    },
    decrypt: function decrypt(data) {
        data = CryptoJS.AES.decrypt(data, SECRET_KEY);

        data = data.toString(CryptoJS.enc.Utf8);

        return data;
    }
});

export const getStorage = (key) => {
    try {
        if (typeof window !== `undefined`) {
            let data = secureStorage.getItem(key);
            if(data){
                if (data.time) {                    
                    if (moment(new Date()).isAfter(moment(data.time.stored).add(data.time.limit, 'hours'))) {                        
                        setStorage(key, null)
                        return false
                    } else {
                        return data.value
                    }
                } else {
                    return data
                }
            } else {
                return false                
            }                    
        }
    } catch (error) {
        console.log(error)
        return false
    }
}

export const setStorage = (key, value, time) => {
    try {
        if (typeof window !== `undefined`) {
            let data = value;
            if (time) {
                data = {
                    value,
                    time: {
                        stored: new Date(),
                        limit: Number(time)
                    }
                }
            }
            secureStorage.setItem(key, data)
        }
    } catch (error) {
        console.log(error)
    }
}

export const clearStorage = () => {
    try {
        if (typeof window !== `undefined`) {
            const currentVersion = getStorage('versionCode');
            window.localStorage.clear();
            window.sessionStorage.clear();
            setStorage('versionCode', currentVersion)
            window.location.replace('/');
        }
    } catch (error) {
        console.log(error)
    }
}