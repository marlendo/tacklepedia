import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import MetaTags from "react-meta-tags";
import Layout from "layouts";
import HeroSlider from "wrappers/hero-slider";
import Banner from "wrappers/banner";
import TabProduct from "wrappers/product";
// import CountDownOne from "wrappers/countdown/CountDownOne";
import FeatureIconTwo from "wrappers/feature-icon/FeatureIconTwo";
import { api, newHttpClient } from "services";
// import BlogFeatured from "wrappers/blog-featured/BlogFeatured";

import { fetchProducts } from "redux/actions/productActions";

const HomeFurniture = ({
  fetchProducts
}) => {

  const [dist, setDist] = useState(false);

  const getDistributor = async () => {
    let data = await newHttpClient(api.inquiryDistributor);
    setDist(data);    
  }

  const getNewArrivals = async () => {
    let data = await newHttpClient(api.inquiryNewArrival, {
      enduser: 'iklan'
    });
    fetchProducts(await data)
  }

  useState(() => {
    getDistributor();
    getNewArrivals();
  }, [])

  return (
    <Fragment>
      <MetaTags>
        <title>TacklePedia | Mancing Mania Mantap</title>
        <meta
          name="tacklepedi"
          content="tacklepedia store menyediakan segala kebutuhan hobi anda"
        />
      </MetaTags>
      <Layout headerTop="visible">
        {/* hero slider */}
        <HeroSlider />

        {/* banner */}
        {
          dist ? (
            <Banner data={dist} />
          ) : (null)
        }

        {/* tab product */}
        <TabProduct spaceBottomClass="pb-100" category={'New Arrival'} />

        {/* countdown */}
        {/* <CountDownOne
          spaceTopClass="pt-115"
          spaceBottomClass="pb-115"
          bgImg="/assets/img/bg/bg-1.jpg"
          dateTime="November 13, 2020 12:12:00"
        /> */}

        {/* feature icon */}
        <FeatureIconTwo spaceTopClass="pt-100" spaceBottomClass="pb-60" />

        {/* blog featured */}
        {/* <BlogFeatured spaceBottomClass="pb-55" /> */}
      </Layout>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: (data) => {
      dispatch(
        fetchProducts(data)
      )
    },    
  };
};

export default connect(null, mapDispatchToProps)(HomeFurniture);
