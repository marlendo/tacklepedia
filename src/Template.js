import PropTypes from "prop-types";
import React, { useEffect, Suspense, lazy } from "react";
import ScrollToTop from "./helpers/scroll-top";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastProvider } from "react-toast-notifications";
import { multilanguage, loadLanguages } from "redux-multilanguage";
import { connect } from "react-redux";
import { BreadcrumbsProvider } from "react-breadcrumbs-dynamic";

// home pages
const HomeFashion = lazy(() => import("backup/pages/home/HomeFashion"));
const HomeFashionTwo = lazy(() => import("backup/pages/home/HomeFashionTwo"));
const HomeFashionThree = lazy(() => import("backup/pages/home/HomeFashionThree"));
const HomeFashionFour = lazy(() => import("backup/pages/home/HomeFashionFour"));
const HomeFashionFive = lazy(() => import("backup/pages/home/HomeFashionFive"));
const HomeFashionSix = lazy(() => import("backup/pages/home/HomeFashionSix"));
const HomeFashionSeven = lazy(() => import("backup/pages/home/HomeFashionSeven"));
const HomeFashionEight = lazy(() => import("backup/pages/home/HomeFashionEight"));
const HomeKidsFashion = lazy(() => import("backup/pages/home/HomeKidsFashion"));
const HomeCosmetics = lazy(() => import("backup/pages/home/HomeCosmetics"));
const HomeFurniture = lazy(() => import("backup/pages/home/HomeFurniture"));
const HomeFurnitureTwo = lazy(() => import("backup/pages/home/HomeFurnitureTwo"));
const HomeFurnitureThree = lazy(() =>
  import("backup/pages/home/HomeFurnitureThree")
);
const HomeFurnitureFour = lazy(() => import("backup/pages/home/HomeFurnitureFour"));
const HomeFurnitureFive = lazy(() => import("backup/pages/home/HomeFurnitureFive"));
const HomeFurnitureSix = lazy(() => import("backup/pages/home/HomeFurnitureSix"));
const HomeFurnitureSeven = lazy(() =>
  import("backup/pages/home/HomeFurnitureSeven")
);
const HomeElectronics = lazy(() => import("backup/pages/home/HomeElectronics"));
const HomeElectronicsTwo = lazy(() =>
  import("backup/pages/home/HomeElectronicsTwo")
);
const HomeElectronicsThree = lazy(() =>
  import("backup/pages/home/HomeElectronicsThree")
);
const HomeBookStore = lazy(() => import("backup/pages/home/HomeBookStore"));
const HomeBookStoreTwo = lazy(() => import("backup/pages/home/HomeBookStoreTwo"));
const HomePlants = lazy(() => import("backup/pages/home/HomePlants"));
const HomeFlowerShop = lazy(() => import("backup/pages/home/HomeFlowerShop"));
const HomeFlowerShopTwo = lazy(() => import("backup/pages/home/HomeFlowerShopTwo"));
const HomeOrganicFood = lazy(() => import("backup/pages/home/HomeOrganicFood"));
const HomeOrganicFoodTwo = lazy(() =>
  import("backup/pages/home/HomeOrganicFoodTwo")
);
const HomeOnepageScroll = lazy(() => import("backup/pages/home/HomeOnepageScroll"));
const HomeGridBanner = lazy(() => import("backup/pages/home/HomeGridBanner"));
const HomeAutoParts = lazy(() => import("backup/pages/home/HomeAutoParts"));
const HomeCakeShop = lazy(() => import("backup/pages/home/HomeCakeShop"));
const HomeHandmade = lazy(() => import("backup/pages/home/HomeHandmade"));
const HomePetFood = lazy(() => import("backup/pages/home/HomePetFood"));
const HomeMedicalEquipment = lazy(() =>
  import("backup/pages/home/HomeMedicalEquipment")
);
const HomeChristmas = lazy(() => import("backup/pages/home/HomeChristmas"));
const HomeBlackFriday = lazy(() => import("backup/pages/home/HomeBlackFriday"));
const HomeBlackFridayTwo = lazy(() =>
  import("backup/pages/home/HomeBlackFridayTwo")
);
const HomeValentinesDay = lazy(() => import("backup/pages/home/HomeValentinesDay"));

// shop pages
const ShopGridStandard = lazy(() => import("backup/pages/shop/ShopGridStandard"));
const ShopGridFilter = lazy(() => import("backup/pages/shop/ShopGridFilter"));
const ShopGridTwoColumn = lazy(() => import("backup/pages/shop/ShopGridTwoColumn"));
const ShopGridNoSidebar = lazy(() => import("backup/pages/shop/ShopGridNoSidebar"));
const ShopGridFullWidth = lazy(() => import("backup/pages/shop/ShopGridFullWidth"));
const ShopGridRightSidebar = lazy(() =>
  import("backup/pages/shop/ShopGridRightSidebar")
);
const ShopListStandard = lazy(() => import("backup/pages/shop/ShopListStandard"));
const ShopListFullWidth = lazy(() => import("backup/pages/shop/ShopListFullWidth"));
const ShopListTwoColumn = lazy(() => import("backup/pages/shop/ShopListTwoColumn"));

// product pages
const Product = lazy(() => import("backup/pages/shop-product/Product"));
const ProductTabLeft = lazy(() =>
  import("backup/pages/shop-product/ProductTabLeft")
);
const ProductTabRight = lazy(() =>
  import("backup/pages/shop-product/ProductTabRight")
);
const ProductSticky = lazy(() => import("backup/pages/shop-product/ProductSticky"));
const ProductSlider = lazy(() => import("backup/pages/shop-product/ProductSlider"));
const ProductFixedImage = lazy(() =>
  import("backup/pages/shop-product/ProductFixedImage")
);

// blog pages
const BlogStandard = lazy(() => import("backup/pages/blog/BlogStandard"));
const BlogNoSidebar = lazy(() => import("backup/pages/blog/BlogNoSidebar"));
const BlogRightSidebar = lazy(() => import("backup/pages/blog/BlogRightSidebar"));
const BlogDetailsStandard = lazy(() =>
  import("backup/pages/blog/BlogDetailsStandard")
);

// other pages
const About = lazy(() => import("backup/pages/other/About"));
const Contact = lazy(() => import("backup/pages/other/Contact"));
const MyAccount = lazy(() => import("backup/pages/other/MyAccount"));
const LoginRegister = lazy(() => import("backup/pages/other/LoginRegister"));

const Cart = lazy(() => import("backup/pages/other/Cart"));
const Wishlist = lazy(() => import("backup/pages/other/Wishlist"));
const Compare = lazy(() => import("backup/pages/other/Compare"));
const Checkout = lazy(() => import("backup/pages/other/Checkout"));

const NotFound = lazy(() => import("backup/pages/other/NotFound"));

const App = (props) => {
  useEffect(() => {
    props.dispatch(
      loadLanguages({
        languages: {
          en: require("./translations/english.json"),
          fn: require("./translations/french.json"),
          de: require("./translations/germany.json")
        }
      })
    );
  });

  return (
    <ToastProvider placement="bottom-left">
      <BreadcrumbsProvider>
        <Router>
          <ScrollToTop>
            <Suspense
              fallback={
                <div className="flone-preloader-wrapper">
                  <div className="flone-preloader">
                    <span></span>
                    <span></span>
                  </div>
                </div>
              }
            >
              <Switch>
                <Route
                  exact
                  path={process.env.PUBLIC_URL + "/"}
                  component={HomeFashion}
                />

                {/* Homepages */}
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion"}
                  component={HomeFashion}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-two"}
                  component={HomeFashionTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-three"}
                  component={HomeFashionThree}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-four"}
                  component={HomeFashionFour}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-five"}
                  component={HomeFashionFive}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-six"}
                  component={HomeFashionSix}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-seven"}
                  component={HomeFashionSeven}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-fashion-eight"}
                  component={HomeFashionEight}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-kids-fashion"}
                  component={HomeKidsFashion}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-cosmetics"}
                  component={HomeCosmetics}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture"}
                  component={HomeFurniture}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-two"}
                  component={HomeFurnitureTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-three"}
                  component={HomeFurnitureThree}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-four"}
                  component={HomeFurnitureFour}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-five"}
                  component={HomeFurnitureFive}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-six"}
                  component={HomeFurnitureSix}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-furniture-seven"}
                  component={HomeFurnitureSeven}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-electronics"}
                  component={HomeElectronics}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-electronics-two"}
                  component={HomeElectronicsTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-electronics-three"}
                  component={HomeElectronicsThree}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-book-store"}
                  component={HomeBookStore}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-book-store-two"}
                  component={HomeBookStoreTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-plants"}
                  component={HomePlants}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-flower-shop"}
                  component={HomeFlowerShop}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-flower-shop-two"}
                  component={HomeFlowerShopTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-organic-food"}
                  component={HomeOrganicFood}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-organic-food-two"}
                  component={HomeOrganicFoodTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-onepage-scroll"}
                  component={HomeOnepageScroll}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-grid-banner"}
                  component={HomeGridBanner}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-auto-parts"}
                  component={HomeAutoParts}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-cake-shop"}
                  component={HomeCakeShop}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-handmade"}
                  component={HomeHandmade}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-pet-food"}
                  component={HomePetFood}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-medical-equipment"}
                  component={HomeMedicalEquipment}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-christmas"}
                  component={HomeChristmas}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-black-friday"}
                  component={HomeBlackFriday}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-black-friday-two"}
                  component={HomeBlackFridayTwo}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/home-valentines-day"}
                  component={HomeValentinesDay}
                />

                {/* Shop pages */}
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-standard"}
                  component={ShopGridStandard}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-filter"}
                  component={ShopGridFilter}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-two-column"}
                  component={ShopGridTwoColumn}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-no-sidebar"}
                  component={ShopGridNoSidebar}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-full-width"}
                  component={ShopGridFullWidth}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-grid-right-sidebar"}
                  component={ShopGridRightSidebar}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-list-standard"}
                  component={ShopListStandard}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-list-full-width"}
                  component={ShopListFullWidth}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/shop-list-two-column"}
                  component={ShopListTwoColumn}
                />

                {/* Shop product pages */}
                <Route
                  path={process.env.PUBLIC_URL + "/product/:id"}
                  render={(routeProps) => (
                    <Product {...routeProps} key={routeProps.match.params.id} />
                  )}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/product-tab-left/:id"}
                  component={ProductTabLeft}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/product-tab-right/:id"}
                  component={ProductTabRight}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/product-sticky/:id"}
                  component={ProductSticky}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/product-slider/:id"}
                  component={ProductSlider}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/product-fixed-image/:id"}
                  component={ProductFixedImage}
                />

                {/* Blog pages */}
                <Route
                  path={process.env.PUBLIC_URL + "/blog-standard"}
                  component={BlogStandard}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/blog-no-sidebar"}
                  component={BlogNoSidebar}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/blog-right-sidebar"}
                  component={BlogRightSidebar}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/blog-details-standard"}
                  component={BlogDetailsStandard}
                />

                {/* Other pages */}
                <Route
                  path={process.env.PUBLIC_URL + "/about"}
                  component={About}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/contact"}
                  component={Contact}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/my-account"}
                  component={MyAccount}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/login-register"}
                  component={LoginRegister}
                />

                <Route
                  path={process.env.PUBLIC_URL + "/cart"}
                  component={Cart}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/wishlist"}
                  component={Wishlist}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/compare"}
                  component={Compare}
                />
                <Route
                  path={process.env.PUBLIC_URL + "/checkout"}
                  component={Checkout}
                />

                <Route
                  path={process.env.PUBLIC_URL + "/not-found"}
                  component={NotFound}
                />

                <Route exact component={NotFound} />
              </Switch>
            </Suspense>
          </ScrollToTop>
        </Router>
      </BreadcrumbsProvider>
    </ToastProvider>
  );
};

App.propTypes = {
  dispatch: PropTypes.func
};

export default connect()(multilanguage(App));
