import PropTypes from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SectionTitle from "components/section-title/SectionTitle";
import ProductGrid from "./ProductGrid";

const TabProductTwo = ({ spaceBottomClass, category }) => {
  return (
    <div className={`product-area ${spaceBottomClass ? spaceBottomClass : ""}`}>
      <div className="container" style={{
        marginTop: 50
      }}>
        <SectionTitle titleText="New Arrivals" positionClass="text-center" />
        <div className="row three-column" style={{
          marginTop: 40
        }}>
          <ProductGrid
            category={category}
            type="new"
            limit={false}
            spaceBottomClass="mb-25"
          />
        </div>
        <div className="view-more text-center mt-20 toggle-btn6 col-12">
          <Link
            className="loadMore6"
            to={process.env.PUBLIC_URL + "/shop-grid-standard"}
          >
            VIEW MORE PRODUCTS
          </Link>
        </div>
      </div>
    </div>
  );
};

TabProductTwo.propTypes = {
  category: PropTypes.string,
  spaceBottomClass: PropTypes.string
};

export default TabProductTwo;
