import React, { useEffect, useState } from "react";
import Swiper from "react-id-swiper";
import HeroSlider from "components/hero-slider";
import { api, newHttpClient } from "services";

const HeroSliderTwo = () => {

  const [heroSliderData, setHeroSliderData] = useState(false);

  useEffect(() => {
    getData();
  }, [])

  const getData = async () => {
    let data = await newHttpClient(api.inquiryIklan, {
      enduser: 'iklan'
    });
    setHeroSliderData(data);     
  }

  const params = {
    effect: "fade",
    loop: true,
    speed: 1000,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: () => (
      <button className="swiper-button-prev ht-swiper-button-nav">
        <i className="pe-7s-angle-left" />
      </button>
    ),
    renderNextButton: () => (
      <button className="swiper-button-next ht-swiper-button-nav">
        <i className="pe-7s-angle-right" />
      </button>
    )
  };

  return (
    <div className="slider-area">
      <div className="slider-active nav-style-1">
        {
          heroSliderData ? (
            <Swiper {...params}>
              {
                heroSliderData.map((single, key) => {
                  return (
                    <HeroSlider
                      sliderClassName={'swiper-slide'}
                      row={single}
                      key={key}
                    />
                  );
                })
              }
            </Swiper>
          ) : null
        }
      </div>
    </div>
  );
};

export default HeroSliderTwo;
