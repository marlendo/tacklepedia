import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { palette } from '@trenaryja/material-color';

const BannerFive = ({
  data
}) => {

  // `http://202.157.186.53/assets/images/upload/${row.gambar}`
  // `http://202.157.186.53/assets/images/upload/${data[ind[1]]['logo']}`

  if (data) {
    return (
      <div className="banner-area hm9-section-padding" style={{
        marginTop: 40
      }}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                {
                  data.map((row, i) => (
                    <div className="col-lg-4 col-md-6" key={i}>
                      <div className="single-banner mb-20">
                        <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                          <img
                            className={'img-right'}
                            src={`http://202.157.186.53/assets/images/upload/${row.logo}`}
                            alt=""
                          />
                        </Link>
                        <div className="banner-content-3 banner-position-hm15-2">
                          <p style={{
                            backgroundColor: 'rgb(225 245 254 / 50%)',
                            padding: 2
                          }}><b>{row.toko}</b></p>
                          <div style={{
                            height: 30
                          }} />
                          <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                            <i className="fa fa-long-arrow-right" />
                          </Link>
                        </div>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      null
    )
  }
};

export default BannerFive;
