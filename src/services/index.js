import axios from 'axios';
import { getStorage, setStorage } from 'helpers/secureStorage';
export const api = require('./api');

const isDev = process.env.NODE_ENV !== 'production';

// const apiVersion = 'v4.0';
let baseUrl = 'http://127.0.0.1:3010/api';
if (!isDev) {
    baseUrl = 'http://202.157.186.53:3010/api';
}

function basicHeader() {
    return {
        "Content-Type": "application/json"
        // "Authorization": "skip"
    }
}

const getPayload = async (name, data) => {
    switch (name) {
        case api.inquiryIklan:
            return {
                headers: basicHeader(),
                method: 'post',
                url: `${baseUrl}/inquiryAds`,
                data
            }
        case api.inquiryDistributor:
            return {
                headers: basicHeader(),
                method: 'post',
                url: `${baseUrl}/tackepedia/inquiryDistributor`,
                data
            }
        case api.inquiryCategoryFixed:
            return {
                headers: basicHeader(),
                method: 'post',
                url: `${baseUrl}/tackepedia/inquiryCategoryFixed`,
                data
            }
        case api.inquiryNewArrival:
            return {
                headers: basicHeader(),
                method: 'post',
                url: `${baseUrl}/inquiryNewArrival`,
                data
            }
        default:
            // eslint-disable-next-line no-throw-literal
            throw 'Error api not found'
    }
}

async function request({
    method, url, headers, data
}) {

    const res = await axios({
        headers,
        method,
        url,
        data
    });

    return res.data;
}

export const httpClient = async (name, data) => {
    const payload = await getPayload(name, data);
    let response = await request(payload);
    return response
}

export const newHttpClient = async (key, payload, timeOut) => {
    let data = getStorage(key);
    if (!data) {
        let response = null
        if (payload) {
            response = await httpClient(key, payload)
        } else {
            response = await httpClient(key)
        }
        if (!response.error) {
            setStorage(key, response.data, timeOut ? Number(timeOut) : 24)
            data = response.data;
        }
    }
    return data;
}

// export const checkValidSession = async () => {
//     const request = await httpClient('checkValidSession', {
//         session: getStorage('key')
//     })
//     if (request.message === 'Session invalid') {
//         return false
//         // return true
//     } else {
//         return true
//     }
// }