import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { multilanguage } from "redux-multilanguage";
import { api, newHttpClient } from "services";

const imgUri = 'http://202.157.186.53:80/assets/images/upload/';

const NavMenu = ({ strings, menuWhiteClass, sidebarMenu }) => {

  const [category, setCategory] = useState(false);
  const [catImg, setCatImg] = useState(false);

  const getCategory = async () => {
    let data = await newHttpClient(api.inquiryCategoryFixed);
    setCategory(data);
    if(data[0].gambar){
      setCatImg(imgUri + data[0].gambar)
    }
  }

  useEffect(() => {
    getCategory()
  }, [])

  return (
    <div
      className={` ${sidebarMenu
        ? "sidebar-menu"
        : `main-menu ${menuWhiteClass ? menuWhiteClass : ""}`
        } `}
    >
      <nav>
        <ul>
          <li>
            <Link to={process.env.PUBLIC_URL + "/"}>
              {strings["home"]}
            </Link>
          </li>
          {
            category ? (
              <li>
                <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                  {" "}
                  {strings["category"]}
                  {sidebarMenu ? (
                    <span>
                      <i className="fa fa-angle-right"></i>
                    </span>
                  ) : (
                      <i className="fa fa-angle-down" />
                    )}
                </Link>
                <ul className="mega-menu">
                  <li>
                    <ul>
                      {
                        category.map((row, i) => (
                          <li key={i.toString()} onMouseEnter={()=> {
                            if(row.gambar){
                              setCatImg(imgUri + row.gambar)
                            }
                          }}>
                            <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                              {row.nama_kategori}
                            </Link>
                          </li>
                        ))
                      }
                    </ul>
                  </li>
                  <li style={{
                    width: '60%'
                  }}>
                    <ul>
                      <li className="mega-menu-img">
                        <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                          <img
                            style={{
                              width: '100%',
                              height: '60vh',
                              objectFit: 'cover'
                            }}
                            src={
                              catImg ? catImg : process.env.PUBLIC_URL + "/assets/img/banner/banner-12.png"
                            }
                            alt=""
                          />
                        </Link>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            ) : (
                null
              )
          }
          <li>
            <Link to={process.env.PUBLIC_URL + "/contact"}>
              {strings["contact_us"]}
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
};

NavMenu.propTypes = {
  menuWhiteClass: PropTypes.string,
  sidebarMenu: PropTypes.bool,
  strings: PropTypes.object
};

export default multilanguage(NavMenu);
