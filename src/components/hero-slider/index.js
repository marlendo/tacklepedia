import { isDark } from "helpers/color";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const HeroSliderTwoSingle = ({ row, sliderClassName }) => {

  const [color, setColor] = useState('#000000');

  useEffect(() => {
    let dark = isDark(row.dominantColor)
    setColor(dark ? '#000000' : '#FFFFFF');
  }, [row.dominantColor])

  return (
    <>
      <div
        style={{
          backgroundColor: row.dominantColor
        }}
        className={`single-slider single-slider-10 slider-height-8 bg-transitions ${sliderClassName ? sliderClassName : ""
          }`}
      >
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-6 d-flex align-items-center">
              <div className="slider-content slider-content-10 slider-animated-2">
                <h3 style={{
                  color
                }} className="animated">{row.nama_iklan}</h3>
                <h1 style={{
                  color
                }} className="animated">{row.deskripsi}</h1>
                <div className="slider-btn btn-hover">
                  <Link
                    style={{
                      color
                    }}
                    className="animated"
                    to={`${process.env.PUBLIC_URL}/`}
                  >
                    SHOP NOW
                </Link>
                </div>
              </div>
            </div>
            <div className="col-12 col-sm-6">
              <div className="slider-singleimg-hm10 slider-animated-2 ml-40 mr-40">
                <img
                  id={`img-slide-${row.gambar}`}
                  className="animated img-fluid"
                  src={row.url}
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

HeroSliderTwoSingle.propTypes = {
  row: PropTypes.object,
  sliderClassName: PropTypes.string
};

export default HeroSliderTwoSingle;
