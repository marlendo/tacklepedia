require("dotenv").config();
const express = require('express');
const bodyParser = require("body-parser");
const cors = require('cors');
const routes = require('./server/router');
const path = require('path');

const app = express();
const port = process.env.PORT;

app.use(cors())

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

// Serve any static files
app.use(express.static(path.join(__dirname, '../tacklepedia/build')));
// Handle React routing, return all requests to React app
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../tacklepedia/build', 'index.html'));
});

app.use((req, res) => {
    res.status(404).json({ message: `${req.originalUrl} not found` });
});

app.listen(port, () => {
    console.log(`tacklepedia middleware started on ${port}`)
})